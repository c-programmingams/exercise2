using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Foodly.Api
{
    //Klasa uruchamiająca serwer webowy i uruchamiająca klasę startową Startup
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        //black whitening
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}