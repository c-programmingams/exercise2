using Foodly.Common.Enums;

namespace Foodly.Data.Sql.DAO
{
    public class Media
    {
        public int MediaId { get; set; }
        public int PostId { get; set; }
        public MediaType MediaType { get; set; }
        public string MediaHref { get; set; }
        public int Order { get; set; }
        
        public virtual Post Post { get; set; }

    }
}