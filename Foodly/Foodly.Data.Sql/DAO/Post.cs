using System;
using System.Collections.Generic;

namespace Foodly.Data.Sql.DAO
{
    public class Post
    {
        public Post()
        {
            PostCategories = new List<PostCategory>();
            Comments = new List<Comment>();
            Medias = new List<Media>();
            PostTags = new List<PostTag>();
            Preferences = new List<Preference>();
        }
        
        public int PostId { get; set; }
        public int UserId { get; set; }
        public string PostDescription { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditionDate { get; set; }
        public int LikesCount { get; set; }
        public int CommentsCount { get; set; }
        public bool IsBannedPost { get; set; }
        public bool IsActivePost { get; set; }
        
        public virtual User User { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PostCategory> PostCategories { get; set; }
        public virtual ICollection<Media> Medias { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
        public virtual ICollection<Preference> Preferences { get; set; }

    }
}