using System;

namespace Foodly.Data.Sql.DAO
{
    public class Preference
    {
        public int PreferenceId { get; set; }
        public int UserId { get; set; }
        public int PostId { get; set; }
        public bool Like { get; set; }
        public DateTime LikeDate { get; set; }
        public virtual Post Post { get; set; }
        public virtual User User { get; set; }

    }
}