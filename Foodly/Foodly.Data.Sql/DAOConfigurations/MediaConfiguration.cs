using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOConfigurations
{
    public class MediaConfiguration: IEntityTypeConfiguration<Media>
    {
        public void Configure(EntityTypeBuilder<Media> builder)
        {
            builder.Property(c => c.MediaHref).IsRequired(); 
            builder.HasOne(x => x.Post)
                .WithMany(x => x.Medias)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.PostId);
            builder.ToTable("Media");
        }
    }
}