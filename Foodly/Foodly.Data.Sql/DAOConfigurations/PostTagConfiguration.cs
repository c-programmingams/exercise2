using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOConfigurations
{
    public class PostTagConfiguration: IEntityTypeConfiguration<PostTag>
    {
        public void Configure(EntityTypeBuilder<PostTag> builder)
        {
            builder.HasOne(x => x.Post)
                .WithMany(x => x.PostTags)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.PostId);
            builder.HasOne(x => x.Tag)
                .WithMany(x => x.PostTags)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.TagId);
            builder.ToTable("PostTag");
        }
    }

}