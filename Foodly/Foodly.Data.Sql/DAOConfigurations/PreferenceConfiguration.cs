using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOConfigurations
{
    public class PreferenceConfiguration: IEntityTypeConfiguration<Preference>
    {
        public void Configure(EntityTypeBuilder<Preference> builder)
        {
            builder.Property(c => c.Like).HasColumnType("tinyint(1)");
            builder.HasOne(x => x.Post)
                .WithMany(x => x.Preferences)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.PostId);
            builder.HasOne(x => x.User)
                .WithMany(x => x.Preferences)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);
            builder.ToTable("Preference");
        }
    }

}