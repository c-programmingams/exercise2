using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOConfigurations
{
    public class UserRelationConfiguration: IEntityTypeConfiguration<UserRelation>
    {
        public void Configure(EntityTypeBuilder<UserRelation> builder)
        {
            builder.Property(c => c.Follow).HasColumnType("tinyint(1)");
            builder.Property(c => c.Block).HasColumnType("tinyint(1)");
            builder.HasOne(x => x.RelatedUser)
                .WithMany(x => x.RelatingUsers)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.RelatedUserId);
            builder.HasOne(x => x.RelatingUser)
                .WithMany(x => x.RelatedUsers)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.RelatingUserId);
            builder.ToTable("UserRelation");
        }
    }

}