using System;
using System.Collections.Generic;
using System.Linq;
using Foodly.Common.Enums;
using Foodly.Common.Extensions;
using Foodly.Data.Sql.DAO;

namespace Foodly.Data.Sql.Migrations
{
    //klasa odpowiadająca za wypełnienie testowymi danymi bazę danych
    public class DatabaseSeed
    {
        private readonly FoodlyDbContext _context;
        
        //wstrzyknięcie instancji klasy FoodlyDbContext poprzez konstruktor
        public DatabaseSeed(FoodlyDbContext context)
        {
            _context = context;
        }
        
        //metoda odpowiadająca za uzupełnienie utworzonej bazy danych testowymi danymi
        //kolejność wywołania ma niestety znaczenie, ponieważ nie da się utworzyć rekordu
        //w bazie dnaych bez znajmości wartości klucza obcego
        //dlatego należy zacząć od uzupełniania tabel,     które nie posiadają kluczy obcych
        //--OFFTOP
        //w przeciwną stronę działa ręczne usuwanie tabel z wypełnionymi danymi w bazie danych
        //należy zacząć od tabel, które posiadają klucze obce, a skończyć na tabelach, które 
        //nie posiadają
        public void Seed()
        {
            //regiony pozwalają na zwinięcie kodu w IDE
            //nie sa dobrą praktyką, kod w danej klasie/metodzie nie powinien wymagać jego zwijania
            //zastosowałem je z lenistwa nie powinno to mieć miejsca 
            #region CreateUsers
            var userList = BuildUserList();            
            //dodanie użytkowników do tabeli User
            _context.User.AddRange(userList);
            //zapisanie zmian w bazie danych
            _context.SaveChanges();
            #endregion
            
            #region CreateUserRelations
            var userRelationList = BuildUserRelationList(userList);
            _context.UserRelation.AddRange(userRelationList);
            _context.SaveChanges();
            #endregion
            
            #region CreateCategories
            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();
            #endregion
            
            #region CreateTags
            var tagList = BuildTagList();
            _context.Tag.AddRange(tagList);
            _context.SaveChanges();
            #endregion
            
            #region CreatePosts
            var postList = BuildPostList(userList);
            var enumerable = postList as Post[] ?? postList.ToArray();
            _context.Post.AddRange(enumerable);
            _context.SaveChanges();
            #endregion

            #region CreatePostCategories
            var postCategoryList = BuildPostCategoryList(enumerable,categoryList);
            _context.PostCategory.AddRange(postCategoryList);
            _context.SaveChanges();
            #endregion

            #region CreatePostTags
            var postTagList = BuildPostTagList(enumerable,tagList);
            _context.PostTag.AddRange(postTagList);
            _context.SaveChanges();
            #endregion

            #region CreateMedias
            var mediaList = BuildMediaList(enumerable, categoryList, postCategoryList);
            _context.Media.AddRange(mediaList);
            _context.SaveChanges();
            #endregion

            #region CreatePreferences
            var preferenceList = BuildPreferenceList(enumerable, userList);
            _context.Preference.AddRange(preferenceList);
            _context.SaveChanges();
            #endregion

            #region CreateComments
            var commentList = BuildCommentList(enumerable, userList);
            _context.Comment.AddRange(commentList);
            _context.SaveChanges();
            #endregion
        }

        private IEnumerable<User> BuildUserList()
        {
            var userList = new List<User>();
            var user = new User()
            {
                UserName = "Salama",
                Email = "salama.hassona@student.po.edu.pl",
                RegistrationDate = DateTime.Now.AddYears(-3),
                EditionDate = DateTime.Now.AddYears(-3),
                BirthDate = new DateTime(1994, 10, 22),
                Gender = Gender.Male,
                IsActiveUser = true,
                IsBannedUser = false,
                FollowersCount = 0,
                FollowingCount = 0,
                PostsCount = 0,
                AccountDescription = "super desc",
                AccountPrivateHref = "http://we.po.opole.pl/",
                IconHref = "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg",
                ThumbnailHref = "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg"
            };
            userList.Add(user);

            var user2 = new User()
            {
                UserName = "Krokodyl",
                Email = "krokodyl@student.po.edu.pl",
                RegistrationDate = DateTime.Now.AddYears(-2),
                EditionDate = DateTime.Now.AddYears(-2),
                BirthDate = new DateTime(1994, 6, 7),
                Gender = Gender.Male,
                IsActiveUser = true,
                IsBannedUser = false,
                FollowersCount = 0,
                FollowingCount = 0,
                PostsCount = 0,
                AccountDescription = "super desc",
                AccountPrivateHref = "http://we.po.opole.pl/",
                IconHref = "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg",
                ThumbnailHref = "https://www.bhphotovideo.com/images/images500x500/LEE_Filters_115R_Peacock_Blue_Color_Effect_688218.jpg"
            };
            userList.Add(user2);
            
            for (int i = 3; i <= 20; i++)
            {
                var user3 = new User
                {
                    UserName = "user" + i,
                    Email = "user" + i + "@student.po.edu.pl",
                    RegistrationDate = DateTime.Now.AddYears(-2),
                    EditionDate = DateTime.Now.AddYears(-2),
                    BirthDate = new DateTime(1994, 6, 7),
                    Gender = i % 2 == 0 ? Gender.Female : Gender.Male,
                    IsActiveUser = true,
                    IsBannedUser = false,
                    FollowersCount = 0,
                    FollowingCount = 0,
                    PostsCount = 0,
                    AccountDescription = "super desc",
                    AccountPrivateHref = "http://we.po.opole.pl/",
                    IconHref = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Solid_black.svg/2000px-Solid_black.svg.png",
                    ThumbnailHref = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Solid_black.svg/2000px-Solid_black.svg.png"
                };
                userList.Add(user3);
            }

            return userList;
        }
        
        private IEnumerable<UserRelation> BuildUserRelationList(
            IEnumerable<User> userList)
        {
            var userRelations = new List<UserRelation>();
            foreach (var user in userList)
            {
                if(user.UserName!="Salama")
                    userRelations.Add(new UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Salama").UserId,
                        RelatingUserId = user.UserId,
                        FollowDate = DateTime.Now,
                        Follow = true
                    });
                if(user.UserName!="Salama"&&user.UserName=="Krokodyl")
                    userRelations.Add(new UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Salama").UserId,
                        RelatingUserId = user.UserId,
                        FollowDate = DateTime.Now,
                        Follow = true
                    });
            }
            foreach (var user in userList)
            {
                if(user.UserName!="Salama"&&user.UserName!="Krokodyl")
                    userRelations.Add(new UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Salama").UserId,
                        RelatingUserId = user.UserId,
                        FollowDate = DateTime.Now,
                        Follow = true
                    });
                if(user.UserName!="Krokodyl"&&user.UserName=="Salama")
                    userRelations.Add(new UserRelation
                    {
                        RelatedUserId = userList.First(x=>x.UserName=="Krokodyl").UserId,
                        RelatingUserId = user.UserId,
                        BlockDate = DateTime.Now,
                        Block = true
                    });
            }
            
            return userRelations;
        }

        private IEnumerable<Category> BuildCategoryList()
        {
            var categoryList = new List<Category>
            {
                new Category
                {
                    CategoryName = "Appetizers & Snacks",
                    ImageHref = "https://www.tablespoon.com/-/media/Images/recipe-hero/appetizer/mini-spinach-lasagna-roll-ups_hero.jpg"
                },
                new Category
                {
                    CategoryName = "Breakfast & Brunch",
                    ImageHref = "https://i.pinimg.com/originals/74/71/e5/7471e50e0d081bd2150dfe399f951928.jpg"
                        
                },
                new Category
                {
                    CategoryName = "Desserts",
                    ImageHref = "https://assets.marthastewart.com/styles/wmax-1500/d38/msl-kitchen-sponge-cakes-0554-md110059/msl-kitchen-sponge-cakes-0554-md110059_horiz.jpg?itok=Bu4cGnhF"
                },
                new Category
                {
                    CategoryName = "Dinner",
                    ImageHref = "https://www.themagicalslowcooker.com/wp-content/uploads/2017/08/easy-spaghetti-dinner-5-of-5.jpg"
                },
                new Category
                {
                    CategoryName = "Drinks",
                    ImageHref = "https://nutritionfacts.org/app/uploads/2017/03/Smoothies.jpeg"
                },
                new Category
                {
                    CategoryName = "Healthy",
                    ImageHref = "https://static.independent.co.uk/s3fs-public/styles/article_small/public/thumbnails/image/2018/01/12/12/healthy-avo-food.jpg"
                },
                new Category
                {
                    CategoryName = "Vegan & Vegetarian",
                    ImageHref = "https://nversia.ru/imgs/news/1521551934_61955526.jpg"
                },
            };
            return categoryList;
        }

        private IEnumerable<Tag> BuildTagList()
        {
            var tagList = new List<Tag>
            {
                new Tag
                {
                    TagName = "sweet1"
                },
                new Tag
                {
                    TagName = "sweet2"
                },
                new Tag
                {
                    TagName = "sweet3"
                },
                new Tag
                {
                    TagName = "sweet4"
                },
                new Tag
                {
                    TagName = "sweet5"
                },
                new Tag
                {
                    TagName = "sweet6"
                },
                new Tag
                {
                    TagName = "sweet7"
                },
                new Tag
                {
                    TagName = "sweet8"
                },
                new Tag
                {
                    TagName = "sweet9"
                },
                new Tag
                {
                    TagName = "sweet10"
                },
            };
            return tagList;
        }

        private IEnumerable<Post> BuildPostList(IEnumerable<User> userList)
        {
            var descs = new List<string>
            {
                "Residence certainly elsewhere something she preferred cordially law. ",
                "Age his surprise formerly mrs perceive few stanhill moderate.",
                "Of in power match on truth worse voice would. Large an it sense shall ",
                "an match learn. By expect it result silent in formal of.",
                "Ask eat questions",
                "described",
                "Cheerful get shutters yet for repeated screened. ",
                "An no am cause hopes at three. Prevent behaved fertile",
                "Extremely nor furniture fat questions now provision incommode preserved.",
                "Our side fail find like now.",
                "An do on frankness so cordially immediate recommend contained.",
                "Imprudence insensible be literature unsatiable do.",
                "Of or imprudence solicitude affronting in mr.",
                "Compass journey he request on suppose limited of or.",
                "She margaret law thoughts proposal formerly.",
                "Speaking ladyship yet scarcely and mistaken end exertion dwelling.",
                "All decisively dispatched instrument particular way",
                "one devonshire. Applauded she sportsman",
                "explained for out objection.",
                "Greatly hearted has who believe.",
            };
            var postList = new List<Post>();
            for (int i = 0; i < 49; i++)
            {
                descs.Shuffle();
                postList.Add(new Post
                {
                    PostDescription = i+1+descs.First(),
                    CreationDate = DateTime.Now,
                    EditionDate = DateTime.Now,
                    CommentsCount = 0,
                    IsActivePost = true,
                    IsBannedPost = false,
                    LikesCount = 0,
                });
            }

            var rand = new Random();
            var userCount = userList.ToList().Count;
            foreach (var post in postList)
            {
                post.UserId = userList.ToList()[rand.Next(userCount)].UserId;
            }
            
            return postList;
        }

        private IEnumerable<PostCategory> BuildPostCategoryList(
            IEnumerable<Post> postList, 
            IEnumerable<Category> categoryList)
        {
            var postCategoryList = new List<PostCategory>();
            postList.ToList().Shuffle();
            categoryList.ToList().Shuffle();
            var rnd = new Random();
            for (int i = 0; i < postList.Count(); i++)
            {
                var index = rnd.Next(0, categoryList.Count());

                postCategoryList.Add(new PostCategory
                {
                    CategoryId = categoryList.ToList()[index].CategoryId,
                    PostId = postList.ToList()[i].PostId
                });
            }
            return postCategoryList;
        }

        private IEnumerable<PostTag> BuildPostTagList(
            IEnumerable<Post> postList,
            IEnumerable<Tag> tagList)
        {
            var postTagList = new List<PostTag>();
            postList.ToList().Shuffle();
            for (int i = 0; i < tagList.Count(); i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    postTagList.Add(new PostTag
                    {
                        TagId = tagList.ToList()[i].TagId,
                        PostId = postList.ToList()[j].PostId
                    });
                }
            }
            return postTagList;
        }

        private IEnumerable<Media> BuildMediaList(
            IEnumerable<Post> postList, 
            IEnumerable<Category> categoryList, 
            IEnumerable<PostCategory> postCategoryList)
        {
            var medias = new Dictionary<string,Func<List<(string,MediaType)>>>
            {
                ["Appetizers & Snacks"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.rickbayless.com/wp-content/uploads/2014/02/ChipsAndGuac.jpg"
                        ,MediaType.Image),
                    ("https://www.tablespoon.com/-/media/Images/recipe-hero/appetizer/mini-spinach-lasagna-roll-ups_hero.jpg",
                        MediaType.Image),
                    ("https://diy-enthusiasts.com/wp-content/uploads/2013/08/fun-appetizers-snacks-kids-party-muffin-cars-600x400.jpg",
                        MediaType.Image),
                    ("https://img1.cookinglight.timeinc.net/sites/default/files/styles/4_3_horizontal_-_1200x900/public/image/2016/04/main/1605p35-bbq-shrimp-toasts.jpg?itok=syp7ZxSt",
                        MediaType.Image),
                    ("https://search.chow.com/thumbnail/840/517/www.chowstatic.com/assets/2015/10/31604_Goat_cheese_stuffed_peppadews_2.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=rJewOzOxQec",
                        MediaType.Video)
                },
                ["Breakfast & Brunch"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.riverwalkbarandgrill.com/wp-content/uploads/bfi_thumb/Riverwalk-Bar-and-Grill-Breakfast-Menu-1920x800-mcpcbb43b6zb3i1vhbzguroermkgt6dse3rfnmn1vk.jpg",
                        MediaType.Image),
                    ("https://cdn3.tmbi.com/secure/RMS/attachments/37/300x300/Buttermilk-Buckwheat-Pancakes_EXPS_BBBZ16_25056_05B_26_2b.jpg",
                        MediaType.Image),
                    ("https://www.kingscross.co.uk/media/P_KXC_L1_DEV_001_Spiritland_N3-800x800.jpg",
                        MediaType.Image),
                    ("https://www.firstwatch.com/wp-content/uploads/2016/12/avacadoToast.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=b6eWM2bJ8cY",
                        MediaType.Video),
                    ("https://www.stylemotivation.com/wp-content/uploads/2014/03/20-Great-Breakfast-Brunch-Recipes-3.jpg",
                        MediaType.Image)
                },
                ["Desserts"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.youtube.com/watch?v=4Cm5qUOS7g0",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=03_EDK9Sn_E",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=XHwosEAcuos",
                        MediaType.Video),
                    ("https://s3.amazonaws.com/video-api-prod/assets/71c4d5d0cf3e468c825945fa82d1eb37/Untitled-1.jpg",
                        MediaType.Image),
                    ("https://www.seriouseats.com/2018/01/20180116-cranachan-vicky-wasik-1-3-625x469.jpg",
                        MediaType.Image),
                    ("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9L4VciYjhLqEvgva0w-vd1HM416FHERafk3is2nul5r9kM_UH",
                        MediaType.Image)
                },
                ["Dinner"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.tasteofhome.com/wp-content/uploads/2017/08/Asparagus-Ham-Dinner_EXPS_THAM17_14208_B11_08_5b-9.jpg",
                        MediaType.Image),
                    ("https://www.cscassets.com/ca/recipes/medium/medium_757.jpg",
                        MediaType.Image),
                    ("https://greatist.com/sites/default/files/7%20Super%20Easy%20Dinners.jpg",
                        MediaType.Image),
                    ("https://static-communitytable.parade.com/wp-content/uploads/2015/12/shared-20.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=dfR_LdA3fPI",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=spHmQb5mA5E",
                        MediaType.Image),
                },
                ["Drinks"]=()=>new List<(string, MediaType)>
                {
                    ("https://img.grouponcdn.com/deal/4GYhYBECmcfjVwYCozmSSaUSRvDK/4G-700x420/v1/c700x420.jpg",
                        MediaType.Image),
                    ("https://www.littlethings.com/app/uploads/2017/03/recipe_card_MakeAheadSmoothies-850x416.jpg",
                        MediaType.Image),
                    ("https://anilakalleshi.com/wp-content/uploads/2018/02/Smoothies-combine-colorful-ingredients.jpg",
                        MediaType.Image),
                    ("https://cdn.shopify.com/s/files/1/0795/1583/products/classic-cleanse-family-shot.jpg?v=1515191340",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=s6lIcPXz23E",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=Kt9wwzKFVUo",
                        MediaType.Video)
                },
                ["Healthy"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.bbcgoodfood.com/sites/default/files/styles/category_retina/public/recipe-collections/collection-image/2017/06/under-200-calorie-collection-pea-shakshuka.jpg?itok=MxK__bZp",
                        MediaType.Image),
                    ("https://www.exploringhealthyfoods.com/wp-content/uploads/2016/10/Lean-Healthy-Meal-Idea-Vegan.jpg",
                        MediaType.Image),
                    ("https://images.media-allrecipes.com/images/65978.jpg",
                        MediaType.Image),
                    ("https://cdn2.momjunction.com/wp-content/uploads/2014/12/Green-Magic.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=wSsgzlavQgM",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=2MvVU_fXtrI",
                        MediaType.Video)
                },
                ["Vegan & Vegetarian"]=()=>new List<(string, MediaType)>
                {
                    ("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQv9g3XUnY7HabOD75syK8SKR9H3lC2PXDBGwtfFOEbhzfPN309",
                        MediaType.Image),
                    ("https://minimalistbaker.com/wp-content/uploads/2015/08/AMAZING-HEALTHY-Vegan-Fried-Rice-with-Crispy-Tofu-vegan-glutenfree-recipe-chinese-friedrice-minimalistbaker-plantbased.jpg",
                        MediaType.Image),
                    ("https://imagesvc.timeincapp.com/v3/mm/image?url=http%3A%2F%2Fimg1.cookinglight.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2Fimage%2F2017%2F03%2Fmain%2Fbeer-brushed-tofu-skewers-barley-1705p105.jpg%3Fitok%3DOdHLIfLx&w=700&q=85",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=kXm5tvnEMgM",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=P_wD2zydD_g",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=cg9zuYkRCkA",
                        MediaType.Video)
                },
            };
            var rand = new Random();
            var mediaList = new List<Media>();
            foreach (var post in postList)
            {
                var postCategory = postCategoryList.First(x => x.PostId == post.PostId);
                var category = categoryList.First(x => x.CategoryId == postCategory.CategoryId);
                var media = medias[category.CategoryName]();
                media.Shuffle();
                var range = rand.Next(1, 4);
                for (int i = 0; i < range; i++)
                {
                    mediaList.Add(new Media
                    {
                        PostId = post.PostId,
                        Order = i+1,
                        MediaHref = media[i].Item1,
                        MediaType = media[i].Item2
                    });
                }                
            }

            return mediaList;
        }

        private IEnumerable<Preference> BuildPreferenceList(
            IEnumerable<Post> postList,
            IEnumerable<User> userList)
        {
            var preferenceList = new List<Preference>();
            foreach (var user in userList)
            {
                if (user.UserName != "Salama")
                {
                    postList.ToList().Shuffle();
                    preferenceList.Add(new Preference
                    {
                        PostId = postList.ToList()[0].PostId,
                        UserId = user.UserId,
                        LikeDate = new DateTime(),
                        Like = true,
                    });
                    postList.ToList()[0].LikesCount += 1;
                }
            }

            return preferenceList;
        }

        private IEnumerable<Comment> BuildCommentList(
            IEnumerable<Post> postList,
            IEnumerable<User> userList)
        {
            var commentList = new List<Comment>();
            var counter = 0;
            var rand = new Random();
            var postsCount = postList.ToList().Count;
            foreach (var user in userList)
            {
                counter++;
                if (user.UserName != "Salama")
                {
                    var postId = rand.Next(postsCount);
                    commentList.Add(new Comment
                    {
                        PostId = postList.ToList()[postId].PostId,
                        UserId = user.UserId,
                        CommentBody = "comment"+counter,
                        CommentDate = DateTime.Now,
                        IsActiveComment = true,
                        IsBannedComment = false
                    });
                    postList.ToList()[postId].CommentsCount += 1;
                }
            }
            return commentList;
        }
    }

}