using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Foodly.Data.Sql;
using Foodly.Data.Sql.Migrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Foodly.Api
{
    public class Startup
    {
        //Reprezentuje zestaw właściwości konfiguracyjnych aplikacji klucz / wartość. (np z pliku appsettings.json)
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        
        //SOLID - http://www.pzielinski.com/?s=ZASADY+S.O.L.I.D
        //Inversion of Control (link zawiera również wyjaśnienie Dependency Injection
        //https://www.c-sharpcorner.com/UploadFile/cda5ba/dependency-injection-di-and-inversion-of-control-ioc/
        public void ConfigureServices(IServiceCollection services)
        {
            //rejestracja DbContextu, użycie providera MySQL i pobranie danych o bazie z appsettings.json
            services.AddDbContext<FoodlyDbContext>(options => options
                .UseMySQL(Configuration.GetConnectionString("FoodlyDbContext")));
            services.AddTransient<DatabaseSeed>();
        }


        // configure pipeline HTTP.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<FoodlyDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }

        }
    }
}