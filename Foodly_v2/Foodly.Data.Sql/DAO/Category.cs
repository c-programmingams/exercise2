﻿using System.Collections.Generic;

namespace Foodly.Data.Sql.DAO
{
    public class Category
    {
        public Category()
        {
            Products = new List< Product>();
        }

        public int CategoryId {get; set; }

        public string CategoryName { get; set; }
        
        public string ImageHref { get; set; } 
        
        public virtual ICollection<Product> Products { get; set; }
        
    }
    
}