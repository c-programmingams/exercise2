﻿using System.Net.Mime;
using Foodly.Common.Enums;

namespace Foodly.Data.Sql.DAO
{
    public class Media        
    {
        public int MediaId { get; set; } // Kolejnosc Medii
        
        public int ProductId { get; set; } // Numer Produktu aby dodac do niego Media
        
        public MediaType MediaType { get; set; } //  Rodzaj Media 
        
        public string MediaHref { get; set; } // media url
        
        public  int Order { get; set; } // Kolejnosc wyswietlen zdjec albo video
        
        public virtual Product Product { get; set; }
        
    }
    
}