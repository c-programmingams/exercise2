﻿using System;
using System.Collections.Generic;

namespace Foodly.Data.Sql.DAO
{
    public class Order
    {

        public Order()
        {
            ProductOrders = new List<ProductOrder>();
            
        }

        public int OrderId { get; set;}
        public int UserId { get; set; }
        public DateTime OrderDate  {get; set;}
        public bool Realisation { get; set; }
        public DateTime RealisationDate { get; set; }
        
        public double Discount { get; set; }
        
        
        
     
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
        public virtual User User { get; set; }
        
    }
}