﻿using System;
using System.Collections.Generic;
using System.Security.Policy;

namespace Foodly.Data.Sql.DAO
{
    public class Product
    {
        public Product()
        {
                
                Comments =  new List<Comment>();
                Medias = new List<Media>();
                Rates = new List<Rate>();
                ProductOrders = new List<ProductOrder>();
                
        }
        
        public int ProductId { get; set; }
        
        
        public int CategoryId { get; set; }
        
        
        public string ProductDescription { get; set; }
        
        public string ProductName { get; set; }
        
        
        public DateTime CreationDate { get; set; }
        
        public double RateCount { get; set; }
        
        public int CommentCount { get; set; }
        
        public bool IsActiveProduct { get; set; }
        
        public int Quantity { get; set; }
        
        public double Price { get; set; }

        
        
        public virtual Category Category { get; set; }
        
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<Media> Medias { get; set; }
        
        public virtual  ICollection<Rate> Rates { get; set; }

        public virtual  ICollection<ProductOrder> ProductOrders { get; set; }

     
    }
}