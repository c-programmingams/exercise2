﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace Foodly.Data.Sql.DAO
{
    public class Rate
    {
        

        public int RateId { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }
        
        public int Count { get; set; }
        
        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        
        
    }
}