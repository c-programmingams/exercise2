﻿using System;
using System.Collections.Generic;

namespace Foodly.Data.Sql.DAO
{
    public class User
    {
        public User()
        {
            Rates = new List<Rate>();
            Comments = new List<Comment>();
            Orders = new List<Order>();
            
        }
        
        public int UserId { get; set; }
        
        public string Username { get; set; }
        
        public string Email { get; set; }
        
        public DateTime RegistrationDate { get; set; }
        
        public DateTime BirthDateTime { get; set; }
        
        public string AvatarHref { get; set; }
        
        public bool IsActiveUser { get; set; }
        
        public bool IsBannedUser { get; set; }
        
        
        
        public virtual ICollection<Rate>Rates { get; set; }
        public virtual ICollection<Comment>Comments { get; set; }
        public virtual ICollection<Order> Orders { get; set; }


    }
    
}