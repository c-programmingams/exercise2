﻿using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Org.BouncyCastle.Crypto.Tls;

namespace Foodly.Data.Sql.DAOconfigurations
{
    public class MediaConfiguration:IEntityTypeConfiguration<Media>
    {
        public void Configure(EntityTypeBuilder<Media> builder)
        {
            builder.Property(c => c.MediaHref).IsRequired();

            builder.HasOne(x => x.Product).WithMany(x => x.Medias).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ProductId);

            builder.ToTable("Media");


        }
    }
}