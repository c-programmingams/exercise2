﻿using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOconfigurations
{
    public class OrderConfiguration:IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
           // builder.Property(c => c.OrderId).IsRequired();
            //builder.HasMany(x => x.ProductOrder).WithOne(x => x.Orders);
            builder.Property(x => x.Realisation).HasColumnType("tinyint(1)");

            builder.HasOne(x => x.User).WithMany(x => x.Orders).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.ToTable("Order");




        }
    }
}