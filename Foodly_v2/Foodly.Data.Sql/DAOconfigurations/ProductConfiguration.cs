﻿using System.Reflection;
using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOconfigurations
{
    public class ProductConfiguration: IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.Property(x => x.ProductDescription).IsRequired();
            builder.Property(x => x.IsActiveProduct).HasColumnType("tinyint(1)");
            builder.HasOne(x => x.Category).WithMany(x => x.Products).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.CategoryId);
            builder.ToTable("Product");

        }
    }
}