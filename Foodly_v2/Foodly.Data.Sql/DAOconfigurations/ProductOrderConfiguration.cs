﻿using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Org.BouncyCastle.Crypto.Tls;

namespace Foodly.Data.Sql.DAOconfigurations
{
    public class ProductOrderConfiguration : IEntityTypeConfiguration<ProductOrder>
    {
        public void Configure(EntityTypeBuilder<ProductOrder> builder)
        {
            builder.HasOne(x => x.Product).WithMany(x => x.ProductOrders).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ProductId);
            
            builder.HasOne(x => x.Order).WithMany(x => x.ProductOrders).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.OrderId);
                
            
            

            builder.ToTable("ProductOrder");
            

        }
    }
}