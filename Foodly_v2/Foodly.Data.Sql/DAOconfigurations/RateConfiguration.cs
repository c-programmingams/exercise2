﻿using Foodly.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Foodly.Data.Sql.DAOconfigurations
{
    public class RateConfiguration: IEntityTypeConfiguration<Rate>
    {
        public void Configure(EntityTypeBuilder<Rate> builder)
        {
            builder.HasOne(x => x.User).WithMany(x => x.Rates).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.UserId);

            builder.HasOne(x => x.Product).WithMany(x => x.Rates).OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ProductId);

            builder.ToTable("Rate");
        }
    }
}