﻿using Foodly.Data.Sql.DAO;
using Foodly.Data.Sql.DAOconfigurations;
using Microsoft.EntityFrameworkCore;

namespace Foodly.Data.Sql
{
    public class FoodlyDbContext : DbContext
    
    {
    public FoodlyDbContext(DbContextOptions<FoodlyDbContext> options) : base(options){} 
    
    public virtual  DbSet<Category>Category { get; set; }
    public virtual  DbSet<Comment> Comment { get; set; }
    public virtual  DbSet<Media> Media { get; set; }
    public virtual  DbSet<Order> Order { get; set; }
    public virtual  DbSet<Product> Product { get; set; }
    public virtual  DbSet<ProductOrder> ProductOrder { get; set; }
    public virtual  DbSet<Rate> Rating { get; set; }
    public virtual  DbSet<User> User { get; set; }




    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.ApplyConfiguration(new CategoryConfigurations());
        builder.ApplyConfiguration(new CommentConfiguration());
        builder.ApplyConfiguration(new MediaConfiguration());
        builder.ApplyConfiguration(new ProductConfiguration());
        builder.ApplyConfiguration(new OrderConfiguration());
        builder.ApplyConfiguration(new ProductOrderConfiguration());
        builder.ApplyConfiguration(new RateConfiguration());
        builder.ApplyConfiguration(new UserConfiguration());
    }
    }
}