﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foodly.Common.Enums;
using Foodly.Common.Extensions;
using Foodly.Data.Sql.DAO;

namespace Foodly.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly FoodlyDbContext _context;

        public DatabaseSeed(FoodlyDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            #region CreateUsers
            var userList = BuildUserList();            
            _context.User.AddRange(userList);
            _context.SaveChanges();
            #endregion
            
            #region CreateCategories
            var categoryList = BuildCategoryList();
            _context.Category.AddRange(categoryList);
            _context.SaveChanges();
            #endregion           
            
            #region CreateProducts
            var productList = BuildProductList(categoryList);
            _context.Product.AddRange(productList);
            _context.SaveChanges();
            #endregion

            #region CreateMedias

            var mediaList = BuildMediaList(productList,categoryList);
            _context.Media.AddRange(mediaList);
            _context.SaveChanges();
            #endregion
            
            #region CreateRates

            var rateList = BuildRates(userList,productList);
            _context.Rating.AddRange(rateList);
            _context.SaveChanges();
            #endregion
            
            #region CreateComments

            var commentList = BuildCommentList(productList, userList);
            _context.Comment.AddRange(commentList);
            _context.SaveChanges();

            #endregion

            #region CreateOrders

            var orderList = BuildOrderList(userList);
            _context.Order.AddRange(orderList);
            _context.SaveChanges();
            #endregion

            #region CreateProductOrders

            var productOrderList = BuildProductOrderList(productList, orderList);
            _context.ProductOrder.AddRange((productOrderList));
            _context.SaveChanges();
            #endregion
        }
        
          private IEnumerable<User> BuildUserList()
        {
            var userList = new List<User>();
            var user = new User()
            {
                Username = "Salama",
                Email = "salama.hassona@student.po.edu.pl",
                RegistrationDate = DateTime.Now.AddYears(-3),
                BirthDateTime = new DateTime(1994, 10, 22),
                AvatarHref = "https://www.google.com",
                IsActiveUser = true,
                IsBannedUser = false,
            };
            userList.Add(user);

            var user2 = new User()
            {
                Username = "Mahbub",
                Email = "mahbub@pwr.wroc.pl",
                RegistrationDate = DateTime.Now.AddYears(-2),
                BirthDateTime = new DateTime(1987, 12, 30),
                AvatarHref = "https://www.google.com",
                IsActiveUser = true,
                IsBannedUser = false,
            };
            userList.Add(user2);
            
            for (int i = 3; i <= 20; i++)
            {
                var user3 = new User
                {
                    Username = "user" + i,
                    Email = "user" + i + "@student.po.edu.pl",
                    RegistrationDate = DateTime.Now.AddYears(-2),
                    BirthDateTime = new DateTime(1994, 10, 22),
                    AvatarHref = "https://www.google.com",
                    IsActiveUser = true,
                    IsBannedUser = false,
                };
                userList.Add(user3);
            }

            
            return userList;
        }
        
          private IEnumerable<Category> BuildCategoryList()
          {
              var categoryList = new List<Category>
              {
                  new Category
                  {
                      CategoryName = "Keyboards",
                      ImageHref = "https://www.tablespoon.com/-/media/Images/recipe-hero/appetizer/mini-spinach-lasagna-roll-ups_hero.jpg"
                  },
                  new Category
                  {
                      CategoryName = "Monitors",
                      ImageHref = "https://i.pinimg.com/originals/74/71/e5/7471e50e0d081bd2150dfe399f951928.jpg"
                        
                  },
                  new Category
                  {
                      CategoryName = "GamingPC",
                      ImageHref = "https://assets.marthastewart.com/styles/wmax-1500/d38/msl-kitchen-sponge-cakes-0554-md110059/msl-kitchen-sponge-cakes-0554-md110059_horiz.jpg?itok=Bu4cGnhF"
                  },
                  new Category
                  {
                      CategoryName = "VideoCards",
                      ImageHref = "https://www.themagicalslowcooker.com/wp-content/uploads/2017/08/easy-spaghetti-dinner-5-of-5.jpg"
                  },
                  new Category
                  {
                      CategoryName = "SoundCards",
                      ImageHref = "https://nutritionfacts.org/app/uploads/2017/03/Smoothies.jpeg"
                  },
                  new Category
                  {
                      CategoryName = "Mice",
                      ImageHref = "https://static.independent.co.uk/s3fs-public/styles/article_small/public/thumbnails/image/2018/01/12/12/healthy-avo-food.jpg"
                  },
                  new Category
                  {
                      CategoryName = "Gaming Notebooks",
                      ImageHref = "https://nversia.ru/imgs/news/1521551934_61955526.jpg"
                  },
              };
              return categoryList;
          }
          
          
          private IEnumerable<Product> BuildProductList(IEnumerable<Category> categoryList)
        {
            var description = new Dictionary<string,Func<List<(string,string)>>>
            {
                ["Keyboards"]=()=>new List<(string,string)>
                {
                    ("Best Razer Keyboard ","Razer" ),
                    ("Best SteelSeries Keyboard ","SteelSeries" ),
                    ("Best Corsair Keyboard ","Corsair" ),
                    ("Best Logitech Keyboard ","Logitech" ),
                    ("Best SCS Keyboard ","SCS" )
                    
                },
                ["Monitors"]=()=>new List<(string,string)>
                {
                    ("Best Aoc Monitor ","Aoc" ),
                    ("Best MSI Monitor ","MSI" ),
                    ("Best Philips Monitor ","Philips" ),
                    ("Best Sharp Monitor ","Sharp" ),
                    ("Best Razer Monitor ","Razer" )
                },
                ["GamingPC"]=()=>new List<(string,string)>
                {
                    ("Best Razer GamingPC ","Razer" ),
                    ("Best MSI GamingPC ","MSI" ),
                    ("Best Xkom GamingPC ","Xkom" ),
                    ("Best Morela GamingPC ","Morela" ),
                    ("Best Apple GamingPC ","Apple" )
                    
                },
                
                ["VideoCards"]=()=>new List<(string,string)>
                {
                    ("Best Msi VideoCard ","Msi" ),
                    ("Best Gainward VideoCard ","Gainward" ),
                    ("Best Gygabyte VideoCard ","Gygabyte" ),
                    ("Best Nvidia VideoCard ","Nvidia" ),
                    ("Best AMD VideoCard ","AMD" )
                    
                },
                
                ["SoundCards"]=()=>new List<(string,string)>
                {
                    ("Best Senhiser SoundCard ","Senhiser" ),
                    ("Best Creative SoundCard ","Creative" ),
                    ("Best Corsair SoundCard ","Corsair" ),
                    ("Best Logitech SoundCard ","Logitech" ),
                    ("Best HyperX SoundCard ","HyperX" )
                    
                },
                ["Mice"]=()=>new List<(string,string)>
                {
                    ("Best Razer Mouse ","Razer" ),
                    ("Best SteelSeries Mouse ","SteelSeries" ),
                    ("Best Corsair Mouse ","Corsair" ),
                    ("Best Logitech Mouse ","Logitech" ),
                    ("Best SCS Mouse ","SCS" )
                    
                },

                ["Gaming Notebooks"]=()=>new List<(string,string)>
                {
                    ("Best Razer Notebook ","Razer" ),
                    ("Best Msi Notebook ","Msi" ),
                    ("Best Dell Notebook ","Dell" ),
                    ("Best Ibm Notebook ","Ibm" ),
                    ("Best Alienware Notebook ","Alienware" )
                    
                },
                
            };
            var rand = new Random();
            var productList = new List<Product>();
            for (int i = 0; i < 49; i++)
            {
                productList.Add(new Product()
                {
               //     ProductDescription = i+1+descs.First(),
                    
                    CreationDate = DateTime.Now,
                    CommentCount = 0,
                    IsActiveProduct = true,
                    Quantity = 1,
                    Price = rand.Next(5000),
                });
            }

            foreach (var product in productList)
            {
                var cat = categoryList.ToList().Shuffle().First();
                product.CategoryId = cat.CategoryId;
                var desc = description[cat.CategoryName]().Shuffle().First();
                product.ProductDescription = desc.Item1 ;
                product.ProductName = desc.Item2 ;
            }

            return productList;
            
        }
          
          
          private IEnumerable<Media> BuildMediaList(
            IEnumerable<Product> products, 
            IEnumerable<Category> categoryList) 
            
        {
            var medias = new Dictionary<string,Func<List<(string,MediaType)>>>
            {
                ["Keyboards"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.rickbayless.com/wp-content/uploads/2014/02/ChipsAndGuac.jpg"
                        ,MediaType.Image),
                    ("https://www.tablespoon.com/-/media/Images/recipe-hero/appetizer/mini-spinach-lasagna-roll-ups_hero.jpg",
                        MediaType.Image),
                    ("https://diy-enthusiasts.com/wp-content/uploads/2013/08/fun-appetizers-snacks-kids-party-muffin-cars-600x400.jpg",
                        MediaType.Image),
                    ("https://img1.cookinglight.timeinc.net/sites/default/files/styles/4_3_horizontal_-_1200x900/public/image/2016/04/main/1605p35-bbq-shrimp-toasts.jpg?itok=syp7ZxSt",
                        MediaType.Image),
                    ("https://search.chow.com/thumbnail/840/517/www.chowstatic.com/assets/2015/10/31604_Goat_cheese_stuffed_peppadews_2.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=rJewOzOxQec",
                        MediaType.Video)
                },
                ["Monitors"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.riverwalkbarandgrill.com/wp-content/uploads/bfi_thumb/Riverwalk-Bar-and-Grill-Breakfast-Menu-1920x800-mcpcbb43b6zb3i1vhbzguroermkgt6dse3rfnmn1vk.jpg",
                        MediaType.Image),
                    ("https://cdn3.tmbi.com/secure/RMS/attachments/37/300x300/Buttermilk-Buckwheat-Pancakes_EXPS_BBBZ16_25056_05B_26_2b.jpg",
                        MediaType.Image),
                    ("https://www.kingscross.co.uk/media/P_KXC_L1_DEV_001_Spiritland_N3-800x800.jpg",
                        MediaType.Image),
                    ("https://www.firstwatch.com/wp-content/uploads/2016/12/avacadoToast.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=b6eWM2bJ8cY",
                        MediaType.Video),
                    ("https://www.stylemotivation.com/wp-content/uploads/2014/03/20-Great-Breakfast-Brunch-Recipes-3.jpg",
                        MediaType.Image)
                },
                ["GamingPC"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.youtube.com/watch?v=4Cm5qUOS7g0",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=03_EDK9Sn_E",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=XHwosEAcuos",
                        MediaType.Video),
                    ("https://s3.amazonaws.com/video-api-prod/assets/71c4d5d0cf3e468c825945fa82d1eb37/Untitled-1.jpg",
                        MediaType.Image),
                    ("https://www.seriouseats.com/2018/01/20180116-cranachan-vicky-wasik-1-3-625x469.jpg",
                        MediaType.Image),
                    ("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9L4VciYjhLqEvgva0w-vd1HM416FHERafk3is2nul5r9kM_UH",
                        MediaType.Image)
                },
                ["VideoCards"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.tasteofhome.com/wp-content/uploads/2017/08/Asparagus-Ham-Dinner_EXPS_THAM17_14208_B11_08_5b-9.jpg",
                        MediaType.Image),
                    ("https://www.cscassets.com/ca/recipes/medium/medium_757.jpg",
                        MediaType.Image),
                    ("https://greatist.com/sites/default/files/7%20Super%20Easy%20Dinners.jpg",
                        MediaType.Image),
                    ("https://static-communitytable.parade.com/wp-content/uploads/2015/12/shared-20.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=dfR_LdA3fPI",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=spHmQb5mA5E",
                        MediaType.Image),
                },
                ["SoundCards"]=()=>new List<(string, MediaType)>
                {
                    ("https://img.grouponcdn.com/deal/4GYhYBECmcfjVwYCozmSSaUSRvDK/4G-700x420/v1/c700x420.jpg",
                        MediaType.Image),
                    ("https://www.littlethings.com/app/uploads/2017/03/recipe_card_MakeAheadSmoothies-850x416.jpg",
                        MediaType.Image),
                    ("https://anilakalleshi.com/wp-content/uploads/2018/02/Smoothies-combine-colorful-ingredients.jpg",
                        MediaType.Image),
                    ("https://cdn.shopify.com/s/files/1/0795/1583/products/classic-cleanse-family-shot.jpg?v=1515191340",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=s6lIcPXz23E",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=Kt9wwzKFVUo",
                        MediaType.Video)
                },
                ["Mice"]=()=>new List<(string, MediaType)>
                {
                    ("https://www.bbcgoodfood.com/sites/default/files/styles/category_retina/public/recipe-collections/collection-image/2017/06/under-200-calorie-collection-pea-shakshuka.jpg?itok=MxK__bZp",
                        MediaType.Image),
                    ("https://www.exploringhealthyfoods.com/wp-content/uploads/2016/10/Lean-Healthy-Meal-Idea-Vegan.jpg",
                        MediaType.Image),
                    ("https://images.media-allrecipes.com/images/65978.jpg",
                        MediaType.Image),
                    ("https://cdn2.momjunction.com/wp-content/uploads/2014/12/Green-Magic.jpg",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=wSsgzlavQgM",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=2MvVU_fXtrI",
                        MediaType.Video)
                },
                ["Gaming Notebooks"]=()=>new List<(string, MediaType)>
                {
                    ("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQv9g3XUnY7HabOD75syK8SKR9H3lC2PXDBGwtfFOEbhzfPN309",
                        MediaType.Image),
                    ("https://minimalistbaker.com/wp-content/uploads/2015/08/AMAZING-HEALTHY-Vegan-Fried-Rice-with-Crispy-Tofu-vegan-glutenfree-recipe-chinese-friedrice-minimalistbaker-plantbased.jpg",
                        MediaType.Image),
                    ("https://imagesvc.timeincapp.com/v3/mm/image?url=http%3A%2F%2Fimg1.cookinglight.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2Fimage%2F2017%2F03%2Fmain%2Fbeer-brushed-tofu-skewers-barley-1705p105.jpg%3Fitok%3DOdHLIfLx&w=700&q=85",
                        MediaType.Image),
                    ("https://www.youtube.com/watch?v=kXm5tvnEMgM",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=P_wD2zydD_g",
                        MediaType.Video),
                    ("https://www.youtube.com/watch?v=cg9zuYkRCkA",
                        MediaType.Video)
                },
            };
            var rand = new Random();
            var mediaList = new List<Media>();
            foreach (var product in products)
            {
                var category = categoryList.First(x => x.CategoryId == product.CategoryId);
                var media = medias[category.CategoryName]();
                media.Shuffle();
                var range = rand.Next(1, 4);
                for (int i = 0; i < range; i++)
                {
                    mediaList.Add(new Media
                    {
                        ProductId = product.ProductId,
                        Order = i+1,
                        MediaHref = media[i].Item1,
                        MediaType = media[i].Item2
                    });
                }                
            }
            
            return mediaList;
        }


          private IEnumerable<Rate> BuildRates(
              IEnumerable<User> users, 
              IEnumerable<Product> products)
          {
              var rand = new Random();
              var rateList = new List<Rate>();
              foreach (var user in users )
              {
                  var productList = products.ToList().Shuffle().Take(15);

                  foreach (var product in productList)
                  {
                      rateList.Add(new Rate
                      {
                          UserId = user.UserId,
                          ProductId = product.ProductId,
                          Count = rand.Next(1,5)
                      });
                      
                      
                      
                  }
              }

              foreach (var product in products)
              {
                  var rates = rateList.Where(x => x.ProductId == product.ProductId);

                  product.RateCount = rates.Average(x => x.Count);
              }
              return rateList;
          }
         
          private IEnumerable<Comment> BuildCommentList(
              IEnumerable<Product> productList,
              IEnumerable<User> userList)
          {
              var commentList = new List<Comment>();
              var counter = 0;
              var rand = new Random();
              var productCount = productList.ToList().Count;
              foreach (var user in userList)
              {
                  counter++;

                  var productId = rand.Next(productCount);
                      commentList.Add(new Comment
                      {
                          ProductId = productList.ToList()[productId].ProductId,
                          UserId = user.UserId,
                          CommentBody = "comment"+counter,
                          CommentDateTime = DateTime.Now,
                          IsActiveComment = true,
                          IsBannedComment = false
                      });
                      productList.ToList()[productId].CommentCount += 1;
                  
              }
              return commentList;
          }

          private IEnumerable<Order> BuildOrderList(
              IEnumerable<User> users)
          {
              var orderList = new List<Order>();
              
              orderList.AddRange(users.Select(user => new Order
              {
                  UserId = user.UserId,
                  OrderDate = DateTime.Now.AddYears(-1),
                  Realisation = false,
                  RealisationDate = DateTime.Now,
                  Discount = 0.0,
              }));

              for (var i = 3; i <= 20; i++)
              {
                  orderList.Add(new Order() {
                      UserId = users.ToList().Shuffle().First().UserId,
                      OrderDate = DateTime.Now.AddYears(-2),
                      Realisation = true,
                      RealisationDate = DateTime.Now,
                      Discount = 3.0, 
                  });
                 
              }
              
              return orderList;
          }



          private IEnumerable<ProductOrder> BuildProductOrderList(
              IEnumerable<Product> productList, IEnumerable<Order> orderList
          )
          {
              
              var productOrderList = new List<ProductOrder>();

               var rand = new Random();
              foreach (var order in orderList)
              {
                  var loopCount = rand.Next(1, 10);
                  for (var i = 0; i<= loopCount; i++ )
                  {
                      var productId = productList
                          .Where(x => !productOrderList
                              .Any(y=>y.OrderId==order.OrderId 
                                      && y.ProductId == x.ProductId))
                          .ToList()
                          .Shuffle()
                          .First()
                          .ProductId;
                      productOrderList.Add(new ProductOrder()
                      {
                          ProductId = productId,
                          OrderId = order.OrderId,
                          Quantity = rand.Next(1,10),
                      });
                  }

              }
              return productOrderList;

          }
       
                
          

         
    }
    
    
}